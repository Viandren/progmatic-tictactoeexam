/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author robb
 */
public class Board3x3 implements Board {

    private List<Cell> cellsOfBoard;
    private int rowNum = 3;
    private static final int GOAL = 3;

    public Board3x3() {
        cellsOfBoard = new ArrayList<>();

        for (int i = 0; i < rowNum; i++) {
            for (int j = 0; j < rowNum; j++) {
                cellsOfBoard.add(new Cell(i, j));
            }
        }

    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        if (rowIdx < rowNum && rowIdx >= 0 && colIdx < rowNum && colIdx >= 0) {

            int idx = (rowIdx) * rowNum + colIdx;

            return cellsOfBoard.get(idx).getCellsPlayer();
        }
        /*
        for (Cell c : cellsOfBoard) {
            if (c.getRow() == rowIdx && c.getCol() == colIdx){
                return c.getCellsPlayer();
            }
        }*/
        throw new CellException(rowIdx, colIdx, "There is no cell on this board with the given parameters!");
    }

    @Override
    public void put(Cell cell) throws CellException {
        boolean cellExists = false;
        for (Cell c : cellsOfBoard) {
            if (c.getRow() == cell.getRow() && c.getCol() == cell.getCol()) {
                if (c.getCellsPlayer().equals(PlayerType.EMPTY)) {
                    c.setCellsPlayer(cell.getCellsPlayer());
                } else {
                    throw new CellException(cell.getRow(), cell.getCol(), "This cell is not empty!");
                }
                cellExists = true;
            }
        }
        if (!cellExists) {
            throw new CellException(cell.getRow(), cell.getCol(), "There is no cell on this board with these coordinates!");
        }
    }

    @Override
    public boolean hasWon(PlayerType p) {

        //sor
        for (int i = 0; i < rowNum; i++) {
            int cnt = 0;
            for (int j = 0; j < rowNum; j++) {
                int idx = i * rowNum + j;
                if (cellsOfBoard.get(idx).getCellsPlayer().equals(p)) {
                    cnt++;
                } else {
                    cnt = 0;
                }
                if (cnt == GOAL) {
                    return true;
                }
            }
        }
        //oszlop
        for (int i = 0; i < rowNum; i++) {
            int cnt = 0;
            for (int j = 0; j < rowNum; j++) {
                int idx = j * rowNum + i;
                if (cellsOfBoard.get(idx).getCellsPlayer().equals(p)) {
                    cnt++;
                } else {
                    cnt = 0;
                }
                if (cnt == GOAL) {
                    return true;
                }
            }
        }
        //jobbra lefelé átló
        int cnt = 0;
        for (int i = 0; i < rowNum; i++) {
            int idx = i * rowNum + i;
            if (cellsOfBoard.get(idx).getCellsPlayer().equals(p)) {
                cnt++;
            } else {
                cnt = 0;
            }
            if (cnt == GOAL) {
                return true;
            }
        }
        //balra lefelé átló
        cnt = 0;
        for (int i = 0; i < rowNum; i++) {
            int idx = i * rowNum + (rowNum - (i + 1));
            if (cellsOfBoard.get(idx).getCellsPlayer().equals(p)) {
                cnt++;
            } else {
                cnt = 0;
            }
            if (cnt == GOAL) {
                return true;
            }
        }

        return false;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> resList = new ArrayList<>();

        for (Cell c : cellsOfBoard) {
            if (c.getCellsPlayer().equals(PlayerType.EMPTY)) {
                resList.add(c);
            }
        }

        return resList;
    }

    public void printBoard() {

        for (Cell c : cellsOfBoard) {
            if (!c.getCellsPlayer().equals(PlayerType.EMPTY)) {
                System.out.print(c.getCellsPlayer() + " ");
            } else {
                System.out.print("_ ");
            }
            if (c.getCol() == rowNum - 1) {
                System.out.println();
            }
        }

    }

}
