/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author robb
 */
public class CustomBoard implements Board {

    private List<Cell> cellsOfBoard;
    private int rowNum;
    private int goal;

    public CustomBoard(int rowNum, int goal) {
        this.rowNum = rowNum;
        this.goal = goal;
        cellsOfBoard = new ArrayList<>();

        for (int i = 0; i < rowNum; i++) {
            for (int j = 0; j < rowNum; j++) {
                cellsOfBoard.add(new Cell(i, j));
            }
        }

    }

    public List<Cell> getCellsOfBoard() {
        return cellsOfBoard;
    }

    public void setCellsOfBoard(List<Cell> cellsOfBoard) {
        this.cellsOfBoard = cellsOfBoard;
    }

    public int getRowNum() {
        return rowNum;
    }

    public int getGoal() {
        return goal;
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        if (rowIdx < rowNum && rowIdx >= 0 && colIdx < rowNum && colIdx >= 0) {

            int idx = (rowIdx) * rowNum + colIdx;

            return cellsOfBoard.get(idx).getCellsPlayer();
        }
        /*
        for (Cell c : cellsOfBoard) {
            if (c.getRow() == rowIdx && c.getCol() == colIdx){
                return c.getCellsPlayer();
            }
        }*/
        throw new CellException(rowIdx, colIdx, "There is no cell on this board with the given parameters!");
    }

    @Override
    public void put(Cell cell) throws CellException {
        boolean cellExists = false;
        for (Cell c : cellsOfBoard) {
            if (c.getRow() == cell.getRow() && c.getCol() == cell.getCol()) {
                if (c.getCellsPlayer().equals(PlayerType.EMPTY)) {
                    c.setCellsPlayer(cell.getCellsPlayer());
                } else {
                    throw new CellException(cell.getRow(), cell.getCol(), "This cell is not empty!");
                }
                cellExists = true;
            }
        }
        if (!cellExists) {
            throw new CellException(cell.getRow(), cell.getCol(), "There is no cell on this board with these coordinates!");
        }
    }

    @Override
    public boolean hasWon(PlayerType p) {

        //sor
        for (int i = 0; i < rowNum; i++) {
            int cnt = 0;
            for (int j = 0; j < rowNum; j++) {
                int idx = i * rowNum + j;
                if (cellsOfBoard.get(idx).getCellsPlayer().equals(p)) {
                    cnt++;
                } else {
                    cnt = 0;
                }
                if (cnt == goal) {
                    return true;
                }
            }
        }
        //oszlop
        for (int i = 0; i < rowNum; i++) {
            int cnt = 0;
            for (int j = 0; j < rowNum; j++) {
                int idx = j * rowNum + i;
                if (cellsOfBoard.get(idx).getCellsPlayer().equals(p)) {
                    cnt++;
                } else {
                    cnt = 0;
                }
                if (cnt == goal) {
                    return true;
                }
            }
        }
        //jobbra lefelé átló
        int cnt = 0;
        for (int i = 0; i < rowNum; i++) {
            int idx = i * rowNum + i;
            if (cellsOfBoard.get(idx).getCellsPlayer().equals(p)) {
                cnt++;
            } else {
                cnt = 0;
            }
            if (cnt == goal) {
                return true;
            }
        }
        //balra lefelé átló
        cnt = 0;
        for (int i = 0; i < rowNum; i++) {
            int idx = i * rowNum + (rowNum - (i + 1));
            if (cellsOfBoard.get(idx).getCellsPlayer().equals(p)) {
                cnt++;
            } else {
                cnt = 0;
            }
            if (cnt == goal) {
                return true;
            }
        }

        return false;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> resList = new ArrayList<>();

        for (Cell c : cellsOfBoard) {
            if (c.getCellsPlayer().equals(PlayerType.EMPTY)) {
                resList.add(c);
            }
        }

        return resList;
    }

    public void printBoard() {
        System.out.print("    ");
        for (int i = 0; i < rowNum; i++) {
            System.out.print(i + " ");
            if (i < 10) {
                System.out.print(" ");
            }

        }
        System.out.println();
        int cnt = 0;
        System.out.print(cnt + "  ");

        for (Cell c : cellsOfBoard) {
            if (!c.getCellsPlayer().equals(PlayerType.EMPTY)) {
                System.out.print(" " + c.getCellsPlayer() + " ");
            } else {
                System.out.print(" _ ");

            }
            if (c.getCol() == rowNum - 1 && c.getRow() != rowNum - 1) {
                System.out.println();
                cnt++;
                System.out.print(cnt + " ");
                if (cnt < 10) {
                    System.out.print(" ");
                }
            }
        }
        System.out.println();
    }

}
