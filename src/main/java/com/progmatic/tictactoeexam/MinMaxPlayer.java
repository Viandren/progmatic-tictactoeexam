/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;

/**
 *
 * @author robb
 */
public class MinMaxPlayer extends AbstractPlayer {

    public MinMaxPlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        return null;
    }

    public Cell nextMove(CustomBoard b) {

        if (b.emptyCells().size() > 0) {

            for (Cell emptyCell : b.emptyCells()) {
                CustomBoard newBoard = new CustomBoard(b.getRowNum(), b.getGoal());

                for (int i = 0; i < b.getRowNum(); i++) {
                    for (int j = 0; j < b.getRowNum(); j++) {
                        PlayerType p;
                        try {
                            p = b.getCell(i, j);
                            newBoard.put(new Cell(i, j, p));
                        } catch (CellException ex) {
                            //nem lesz ilyen
                        }

                    }
                }

                Cell newCell = new Cell(emptyCell.getRow(), emptyCell.getCol(), myType);
                try {
                    newBoard.put(newCell);
                } catch (CellException ex) {
                    //nem lehetséges ilyen
                }
                if (this.movesToWin(b) > this.movesToWin(newBoard)) {
                    return newCell;
                }
            }
        }
        Cell resCell = new Cell(b.emptyCells().get((int) (Math.random() * b.emptyCells().size())).getRow(), b.emptyCells().get(0).getCol(), myType);
        return resCell;

    }

    public int movesToWin(CustomBoard b) {

        //get longest chain with an open end or an opening in between
        /*
        //oszlop
        int maxCnt = 0;
        ArrayList<Cell> chain = new ArrayList<>();
        ArrayList<Cell> longestChain = new ArrayList<>();
        boolean containsEmpty = false;
        int beginIdx = 0;
        int endIdx = 0;
        for (int i = 0; i < b.getRowNum(); i++) {
            int cnt = 0;
            for (int j = 0; j < b.getRowNum(); j++) {
                int idx = i * b.getRowNum() + j;

                if (b.getCellsOfBoard().get(idx).getCellsPlayer().equals(myType)) {
                    chain.add(b.getCellsOfBoard().get(idx));
                    if (cnt == 0) {
                        beginIdx = idx;
                    }
                    cnt++;
                } else if (b.getCellsOfBoard().get(idx).getCellsPlayer().equals(PlayerType.EMPTY)) {
                    cnt = 0;
                    if (chain.size() > 0 && !chain.get(chain.size() - 1).getCellsPlayer().equals(PlayerType.EMPTY)) {
                        containsEmpty = true;
                        chain.add(b.getCellsOfBoard().get(idx));
                    } else {
                        cnt = 0;
                    }
                    
                } else {
                    cnt = 0;
                }
                if (cnt == 0) {
                    if (!chain.isEmpty() && chain.size() > longestChain.size()) {
                        if (containsEmpty || b.getCellsOfBoard().get(beginIdx - Math.min(1, beginIdx)).getCellsPlayer().equals(PlayerType.EMPTY)) {
                            longestChain.clear();
                            longestChain.addAll(chain);
                            chain.clear();
                            containsEmpty = false;
                        } else {
                            chain.clear();
                            containsEmpty = false;
                        }
                    }
                }
            }
        }

        return longestChain.size();

         */
        PlayerType[][] arr = new PlayerType[b.getRowNum()][b.getRowNum()];
        for (int i = 0; i < b.getRowNum(); i++) {
            for (int j = 0; j < b.getRowNum(); j++) {
                int idx = i * b.getRowNum() + j;
                arr[i][j] = b.getCellsOfBoard().get(idx).getCellsPlayer();
            }
        }

        int cnt = 0;
        int maxCnt = 0;
        for (int i = 0; i < b.getRowNum(); i++) {
            cnt = 0;
            for (int j = 0; j < b.getRowNum(); j++) {
                if (arr[i][j] == myType) {
                    cnt++;
                    if (maxCnt < cnt) {
                        maxCnt = cnt;
                    }
                } else {
                    cnt = 0;
                }

            }
        }
        for (int i = 0; i < b.getRowNum(); i++) {
            cnt = 0;
            for (int j = 0; j < b.getRowNum(); j++) {
                if (arr[j][i] == myType) {
                    cnt++;
                    if (maxCnt < cnt) {
                        maxCnt = cnt;
                    }
                } else {
                    cnt = 0;
                }

            }
        }
        cnt = 0;
        for (int i = 0; i < b.getRowNum(); i++) {

            if (arr[i][i] == myType) {
                cnt++;
                if (maxCnt < cnt) {
                    maxCnt = cnt;
                }
            } else {
                cnt = 0;
            }

        }
        for (int i = 0; i < b.getRowNum(); i++) {

            if (arr[i][b.getRowNum() - (i + 1)] == myType) {
                cnt++;
                if (maxCnt < cnt) {
                    maxCnt = cnt;
                }
            } else {
                cnt = 0;
            }

        }

        return b.getGoal() - maxCnt;
    }
    
    public ArrayList<Cell> getLongestOpenChain(CustomBoard b){
        int maxCnt = 0;
        ArrayList<Cell> chain = new ArrayList<>();
        ArrayList<Cell> longestChain = new ArrayList<>();
        boolean containsEmpty = false;
        int beginIdx = 0;
        int endIdx = 0;
        for (int i = 0; i < b.getRowNum(); i++) {
            int cnt = 0;
            for (int j = 0; j < b.getRowNum(); j++) {
                
                int idx = i * b.getRowNum() + j;

                if (b.getCellsOfBoard().get(idx).getCellsPlayer().equals(myType)) {
                    chain.add(b.getCellsOfBoard().get(idx));
                    if (cnt == 0) {
                        beginIdx = idx;
                    }
                    cnt++;
                } else if (b.getCellsOfBoard().get(idx).getCellsPlayer().equals(PlayerType.EMPTY)) {
                    cnt = 0;
                    if (chain.size() > 0 && !chain.get(chain.size() - 1).getCellsPlayer().equals(PlayerType.EMPTY)) {
                        containsEmpty = true;
                        chain.add(b.getCellsOfBoard().get(idx));
                    } else {
                        cnt = 0;
                    }
                    
                } else {
                    cnt = 0;
                }
                if (i == chain.size()-1){
                    cnt = 0;
                }
                if (cnt == 0) {
                    if (chain.size()>1 && chain.get(chain.size()-1).getCellsPlayer().equals(PlayerType.EMPTY)){
                        chain.remove(chain.size()-1);
                    }
                    if (!chain.isEmpty() && chain.size() > longestChain.size()) {
                        if (containsEmpty || b.getCellsOfBoard().get(beginIdx - Math.min(1, beginIdx)).getCellsPlayer().equals(PlayerType.EMPTY)) {
                            longestChain.clear();
                            longestChain.addAll(chain);
                            chain.clear();
                            containsEmpty = false;
                        } else {
                            chain.clear();
                            containsEmpty = false;
                        }
                    }
                }
            }
        }
        
        for (Cell cell : longestChain) {
            System.out.println(cell.getRow()+"|"+cell.getCol());
        }
        
        return longestChain;
    }

}
