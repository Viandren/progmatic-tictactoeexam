/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author robb
 */
public class Human extends AbstractPlayer{

    public Human(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Cell nextMove(CustomBoard b) {
        System.out.println("Te jössz!");
            String answer = "";
            int x = 0, y = 0;
            Scanner sc = new Scanner(System.in);
            boolean megvan = false;
            while(!megvan){
            while (answer.equals("")) {
                System.out.println("X coord:");
                answer = sc.nextLine();
                if (isLegitNumber(answer, 0, b.getRowNum() - 1)) {
                    x = Integer.parseInt(answer);
                } else {
                    System.out.println("Ez nem jó szám! Kérlek adj meg újat!");
                }
            }
            answer = "";
            while (answer.equals("")) {
                System.out.println("Y coord:");
                answer = sc.nextLine();
                if (isLegitNumber(answer, 0, b.getRowNum() - 1)) {
                    y = Integer.parseInt(answer);
                } else {
                    System.out.println("Ez nem jó szám! Kérlek adj meg újat!");
                }
            }
            if (b.getCellsOfBoard().get(y*b.getRowNum()+x).getCellsPlayer().equals(PlayerType.EMPTY)){
                megvan = true;
            } else {
                System.out.println("Ez a hely már nem üres!");
                answer = "";
            }
            }
            return new Cell(y, x, myType);
    }
    
    public boolean isLegitNumber(String str, int floor, int roof) {
        int num = 0;
        try {
            num = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return false;
        }
        if (num > roof || num < floor) {
            return false;
        }
        return true;
    }
    
    
    /**
     * 
     * Ez kéne a magasabb szintű taktikázáshoz, de még vízszintesen sem működik jól :/
     * 
     * @param b
     * @return 
     */
    
    
    public ArrayList<Cell> getLongestOpenChain(CustomBoard b){
        int maxCnt = 0;
        ArrayList<Cell> chain = new ArrayList<>();
        ArrayList<Cell> longestChain = new ArrayList<>();
        boolean containsEmpty = false;
        int beginIdx = 0;
        int endIdx = 0;
        for (int i = 0; i < b.getRowNum(); i++) {
            int cnt = 0;
            for (int j = 0; j < b.getRowNum(); j++) {
                
                int idx = i * b.getRowNum() + j;

                if (b.getCellsOfBoard().get(idx).getCellsPlayer().equals(myType)) {
                    chain.add(b.getCellsOfBoard().get(idx));
                    if (cnt == 0) {
                        beginIdx = idx;
                    }
                    endIdx=idx;
                    cnt++;
                } else if (b.getCellsOfBoard().get(idx).getCellsPlayer().equals(PlayerType.EMPTY)) {
                    if (chain.size() > 0 && !chain.get(chain.size() - 1).getCellsPlayer().equals(PlayerType.EMPTY) &&
                            b.getCellsOfBoard().get(idx+1).getCellsPlayer().equals(myType) && !containsEmpty) {
                        containsEmpty = true;
                        chain.add(b.getCellsOfBoard().get(idx));
                    } else {
                        cnt = 0;
                    }
                    
                } else {
                    cnt = 0;
                }
                if (i == b.getRowNum()-1){
                    cnt = 0;
                }
                if (cnt == 0) {
                    /*
                    if (chain.size()>1 && chain.get(chain.size()-1).getCellsPlayer().equals(PlayerType.EMPTY)){
                        chain.remove(chain.size()-1);
                    }
                    */
                    if (chain.size() > longestChain.size()) {
                        if (containsEmpty || (beginIdx % b.getRowNum() != 0 && b.getCellsOfBoard().get(beginIdx - Math.min(1, beginIdx)).getCellsPlayer().equals(PlayerType.EMPTY)) ||
                                (i != b.getRowNum()-1 && b.getCellsOfBoard().get(idx+1).getCellsPlayer().equals(PlayerType.EMPTY))) {
                            longestChain.clear();
                            longestChain.addAll(chain);
                            chain.clear();
                            containsEmpty = false;
                        } else {
                            chain.clear();
                            containsEmpty = false;
                        }
                    } else {
                            chain.clear();
                            containsEmpty = false;
                        }
                }
            }
        }
        
        for (Cell cell : longestChain) {
            System.out.println(cell.getRow()+"|"+cell.getCol());
        }
        
        return longestChain;
    }

    
}
