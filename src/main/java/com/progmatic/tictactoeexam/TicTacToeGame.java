/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author robb
 */
public class TicTacToeGame {

    public TicTacToeGame() {
        doGame();
    }

    public static void main(String[] args) {

        TicTacToeGame tg = new TicTacToeGame();
    }

    public void doGame() {

        Scanner sc = new Scanner(System.in);
        String answer = "";
        System.out.println("Mekkora táblán szeretnél játszani? Adj meg egy oldalhosszt!");
        while (answer.equals("")) {
            answer = sc.nextLine();
            if (!isLegitNumber(answer, 3, 99)) {
                answer = "";
                System.out.println("Adj meg egy 3 és 100 közötti számot, kérlek!");
            }
        }
        int oldalhossz = Integer.parseInt(answer);
        int goal = 3;
        if (oldalhossz > 3) {
            System.out.println("Mekkora sorozat érjen győzelmet?");
            answer = "";
            while (answer.equals("")) {
                answer = sc.nextLine();
                if (!isLegitNumber(answer, 3, oldalhossz)) {
                    answer = "";
                    System.out.println("Adj meg egy 3 és " + oldalhossz + " közötti számot, kérlek!");
                }
            }
            goal = Integer.parseInt(answer);
        }
        System.out.println(oldalhossz);
        System.out.println(goal);

        CustomBoard b = new CustomBoard(oldalhossz, goal);

        System.out.println("A tábla kész.");
        System.out.println("Melyik figurával akarsz lenni? X/O");
        answer = "";
        while (answer.equals("")) {
            answer = sc.nextLine();
            if (!answer.toLowerCase().equals("x") && !answer.toLowerCase().equals("o")) {
                System.out.println("Kérlek X és O közül válassz!");
                answer = "";
            }
        }
        
        Human humanPlayer = new Human(PlayerType.valueOf(answer.toUpperCase()));
        MinMaxPlayer AI;
        if (humanPlayer.getMyType().equals(PlayerType.O)){
            AI = new MinMaxPlayer(PlayerType.X);
        } else {
            AI = new MinMaxPlayer(PlayerType.O);
        }
        
        int x = 0, y = 0;

        
        
        
        
        while (!b.hasWon(PlayerType.O) && !b.hasWon(PlayerType.X)) {
            
            if (humanPlayer.getMyType().equals(PlayerType.O)){
                try {
                    b.put(humanPlayer.nextMove(b));
                    b.put(AI.nextMove(b));
                } catch (CellException ex) {
                }
            } else {
                try {
                    b.put(AI.nextMove(b));
                    b.printBoard();
                    b.put(humanPlayer.nextMove(b));
                } catch (CellException ex) {
                }
            }
            
            
            b.printBoard();
            System.out.println("A leghosszabb sorozatod, ami még nyitott:");
            humanPlayer.getLongestOpenChain(b);
            // System.out.println("Ennyi lépésre: "+new MiniMaxPlayer(humanPlayersType).movesToWin(b));
        }

    }

    public boolean isLegitNumber(String str, int floor, int roof) {
        int num = 0;
        try {
            num = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return false;
        }
        if (num > roof || num < floor) {
            return false;
        }
        return true;
    }

}
