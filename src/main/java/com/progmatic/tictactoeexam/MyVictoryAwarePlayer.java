/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author robb
 */
public class MyVictoryAwarePlayer extends AbstractPlayer{

    public MyVictoryAwarePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        
        //keres egy nyerő cellát
        
        if (b.emptyCells().size() > 0){
            
            for (Cell emptyCell : b.emptyCells()) {
                Board3x3 newBoard = new Board3x3();
                
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        PlayerType p;
                        try {
                            p = b.getCell(i, j);
                            newBoard.put(new Cell(i,j,p));
                        } catch (CellException ex) {
                           //nem lesz ilyen
                        }
                        
                    }
                }
                
                Cell newCell = new Cell(emptyCell.getRow(),emptyCell.getCol(),myType);
                try {
                    newBoard.put(newCell);
                } catch (CellException ex) {
                    //nem lehetséges ilyen
                }
                if (newBoard.hasWon(myType)){
                    return newCell;
                }
            }
            
            //nem talált nyerő cellát, tesz az első üresre
            
            Cell resCell = new Cell(b.emptyCells().get(0).getRow(),b.emptyCells().get(0).getCol(),myType);
            return resCell;
        }
        
        
        
        
        
        return null;
    }
    
    
    public Cell nextMove(CustomBoard b) {
        
        //keres egy nyerő cellát
        
        if (b.emptyCells().size() > 0){
            
            for (Cell emptyCell : b.emptyCells()) {
                Board3x3 newBoard = new Board3x3();
                
                for (int i = 0; i < b.getRowNum(); i++) {
                    for (int j = 0; j < b.getRowNum(); j++) {
                        PlayerType p;
                        try {
                            p = b.getCell(i, j);
                            newBoard.put(new Cell(i,j,p));
                        } catch (CellException ex) {
                           //nem lesz ilyen
                        }
                        
                    }
                }
                
                Cell newCell = new Cell(emptyCell.getRow(),emptyCell.getCol(),myType);
                try {
                    newBoard.put(newCell);
                } catch (CellException ex) {
                    //nem lehetséges ilyen
                }
                if (newBoard.hasWon(myType)){
                    return newCell;
                }
            }
            
            //nem talált nyerő cellát, tesz az első üresre
            
            Cell resCell = new Cell(b.emptyCells().get(0).getRow(),b.emptyCells().get(0).getCol(),myType);
            return resCell;
        }
        
        
        
        
        
        return null;
    }
}
